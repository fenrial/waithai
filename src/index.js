import React from "react";
import ReactDOM from "react-dom";

import "./styles/style.sass";

// components
import Header from "./components/header/header"

const App = () => {
  return <Header></Header>;
};

ReactDOM.render(<App/>, document.getElementById("App"));